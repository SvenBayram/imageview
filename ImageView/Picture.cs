﻿using System;
using System.Threading;
using System.Windows.Media.Imaging;

namespace ImageView
{
    /// <summary>
    /// The <see cref="Picture"/> object provides a <see cref="CachedBitmap"/> and can load itself in a seperate thread. It needs only a URL from an Image.
    /// </summary>
    internal class Picture
    {
        private CachedBitmap bitmapcached;
        private bool loaded = false;
        private Thread loadthread;
        private string url;

        /// <summary>
        /// Creates a picture object and load itself into the memory in a seperate thread.
        /// </summary>
        /// <param name="url">URL-String from an image data.</param>
        public Picture(string url)
        {
            this.url = url;
            loadthread = new Thread(LoadThread);
            loadthread.SetApartmentState(ApartmentState.STA);
            loadthread.Start();
        }

        /// <summary>
        /// Set all objects to <see langword="null"/>.
        /// </summary>
        public void Clear()
        {
            bitmapcached = null;
            url = null;
            loadthread = null;
        }

        /// <summary>
        /// Return the CachedBitmap.
        /// </summary>
        /// <returns></returns>
        public CachedBitmap GetImage()
        {
            return bitmapcached;
        }

        /// <summary>
        /// Returns <see langword="true"/> if the object is loaded.
        /// </summary>
        /// <returns></returns>
        public bool IsLoaded()
        {
            return loaded;
        }

        /// <summary>
        ///Method for the loading-thread. It takes the URL and load it into a <see cref="CachedBitmap"/>.
        /// </summary>
        private void LoadThread()
        {
            bitmapcached = new CachedBitmap(new BitmapImage(new Uri(url)), BitmapCreateOptions.IgnoreImageCache, BitmapCacheOption.OnLoad);
            bitmapcached.Freeze();
            loaded = true;
        }
    }
}