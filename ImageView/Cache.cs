﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Media.Imaging;

namespace ImageView
{
    internal class Cache
    {
        private Picture[] cache;
        private int current = 0;
        private int offset = 0;
        private int size;
        private List<string> urls;
        public Cache(int size, List<string> urls)
        {
            this.size = size;
            this.urls = urls;
            cache = new Picture[size];
        }

        public void ClearAll()
        {
            for (int i = 0; i < size; i++)
            {
                cache[i].Clear();
                cache[i] = null;
            }
            cache = null;
            urls = null;
        }

        public CachedBitmap GetImg()
        {
            while (!cache[Math.Abs((current + size) % size)].IsLoaded())
            {
                Thread.Sleep(1);
            }
            return cache[Math.Abs((current + size) % size)].GetImage();
        }

        public CachedBitmap GetImg(int i)
        {
            while (!cache[Math.Abs(((current + i) + size) % size)].IsLoaded())
            {
                Thread.Sleep(1);
            }
            return cache[Math.Abs(((current + i) + size) % size)].GetImage();
        }

        public void Init(int offset)
        {
            this.offset = offset;
            for (int i = 0; i < size; i++)
            {
                cache[Math.Abs((i + size - (size / 2)) % size)] = new Picture(urls[Math.Abs((i + offset + urls.Count - (size / 2)) % urls.Count)]);
            }
        }
        public void Next()
        {
            current++;
            cache[Math.Abs((current + size + (size / 2)) % size)].Clear();
            cache[Math.Abs((current + size + (size / 2)) % size)] = null;
            cache[Math.Abs((current + size + (size / 2)) % size)] = new Picture(urls[Math.Abs((current + urls.Count + (size / 2) + offset) % urls.Count)]);
        }

        public void Previous()
        {
            current--;
            cache[Math.Abs((current + size + (size / 2) + 1) % size)].Clear();
            cache[Math.Abs((current + size + (size / 2) + 1) % size)] = null;
            cache[Math.Abs((current + size + (size / 2) + 1) % size)] = new Picture(urls[Math.Abs((current + urls.Count - (size / 2) + offset) % urls.Count)]);
        }
    }
}