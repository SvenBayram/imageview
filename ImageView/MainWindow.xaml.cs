﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace ImageView
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int cachesize = 3;
        private Thread collectThread;
        private Cache imagecache;
        private List<string> imageURLs = new List<string>();
        private int pictureoffset;
        public MainWindow()
        {
            InitializeComponent();
            collectThread = new Thread(Collect);
            collectThread.Start();
        }

        public List<string> ImageURLs { get => imageURLs; set => imageURLs = value; }
        public void Collect()
        {
            while (true)
            {
                GC.Collect();
                Thread.Sleep(1000);
            }
        }

        private void Btn_Next_Click(object sender, RoutedEventArgs e)
        {
            imagecache.Next();
            ShowImage();
        }

        private void Btn_Open_Dir_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string folderpath;
                folderpath = fbd.SelectedPath;
                DirectoryInfo di = new DirectoryInfo(folderpath);
                imageURLs.Clear();
                if (imagecache != null)
                {
                    imagecache.ClearAll();
                    imagecache = null;
                }
                WalkFolder(di);
                imagecache = new Cache(cachesize, imageURLs);
                imagecache.Init(pictureoffset);
                ShowImage();
                fbd.Dispose();
            }
        }

        private void Btn_Open_Pic_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string tempfile = ofd.FileName;
                string tempdir;
                tempdir = tempfile.Remove(tempfile.Length - ofd.SafeFileName.Length);
                DirectoryInfo di = new DirectoryInfo(tempdir);
                imageURLs.Clear();
                if (imagecache != null)
                {
                    imagecache.ClearAll();
                    imagecache = null;
                }
                WalkFolder(di);
                for (int k = 0; k < ImageURLs.Count; k++)
                {
                    if (tempfile == ImageURLs[k])
                    {
                        pictureoffset = k;
                    }
                }
                imagecache = new Cache(cachesize, imageURLs);
                imagecache.Init(pictureoffset);
                ShowImage();
                ofd.Dispose();
            }
        }

        private void Btn_Previous_Click(object sender, RoutedEventArgs e)
        {
            imagecache.Previous();
            ShowImage();
        }

        private void Refresh()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void ShowImage()
        {
            ImageBox.Source = imagecache.GetImg();
        }
        private void WalkFolder(DirectoryInfo di)
        {
            try
            {
                bool check = CheckBox1.IsChecked ?? false;
                if (check)
                {
                    foreach (DirectoryInfo subdir in di.GetDirectories())
                    {
                        WalkFolder(subdir);
                    }
                }
                foreach (FileInfo fi in di.GetFiles())
                {
                    if (fi.Extension.ToLower() == ".png" ||
                        fi.Extension.ToLower() == ".bmp" ||
                        fi.Extension.ToLower() == ".jpeg" ||
                        fi.Extension.ToLower() == ".tif" ||
                        fi.Extension.ToLower() == ".jpg")
                    {
                        ImageURLs.Add(fi.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                imagecache.Next();
                ShowImage();
            }
            if (e.Key == Key.Left)
            {
                imagecache.Previous();
                ShowImage();
            }
        }
    }
}